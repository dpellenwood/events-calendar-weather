<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://tri.be
 * @since      1.0.0
 *
 * @package    Events_Calendar_Weather
 * @subpackage Events_Calendar_Weather/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Events_Calendar_Weather
 * @subpackage Events_Calendar_Weather/public
 * @author     Modern Tribe <info@moderntribe.com>
 */
class Events_Calendar_Weather_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $events_calendar_weather    The ID of this plugin.
	 */
	private $events_calendar_weather;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $events_calendar_weather       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $events_calendar_weather, $version ) {

		$this->events_calendar_weather = $events_calendar_weather;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Events_Calendar_Weather_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Events_Calendar_Weather_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->events_calendar_weather, plugin_dir_url( __FILE__ ) . 'css/events-calendar-weather-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Events_Calendar_Weather_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Events_Calendar_Weather_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->events_calendar_weather, plugin_dir_url( __FILE__ ) . 'js/events-calendar-weather-public.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Add premium plugin paths for each file in the templates array
	 *
	 * @param    $template_paths array
	 * @return   array
	 * @since    1.0.0
	 */
	public function template_paths( $template_paths = array() ) {

		//$template_paths[] = plugin_dir_path( __FILE__ );
		array_unshift( $template_paths, plugin_dir_path( __FILE__ ) );
		return $template_paths;

	}


	/**
	 * Return a month of weather data
	 *
	 * @return  $weather    array   An array of weather for each day in the given month.
	 * @since   1.0.0
	 */
	public function get_monthly_forecast( $eventDate = null ) {

		// If we're not sending in a date, then see if the global is available.
		if ( empty( $eventDate ) ) {

			global $eventDate;

			// If this is an AJAX request, the date is in the request
			if ( defined( 'DOING_AJAX' ) && 'tribe_calendar' == $_REQUEST['action'] ) {
				$eventDate = esc_attr( $_REQUEST['eventDate'] );
			}

		}

		// We're only doing this for the month view.  Nothing else.
		if ( ! tribe_is_month() ) {
			return null;
		}

		// The default view doesn't set the global date.
		if ( empty( $eventDate ) ) {
			$eventDate = date( 'Y-m' );
		}

		$weather[$eventDate] = false;
		$location = tribe_get_option( 'weatherLatLong' );

		if ( ! empty( $location ) ) {

			$loc_transient_name = 'ecw_data_' . md5( $location[2] );

			// Do we already have a forecast for this location?
			$weather = get_transient( $loc_transient_name );

			// If we don't have a forecast for this month at this location then get it!
			if ( empty( $weather[$eventDate] ) ) {

				// get this month's forecast
				$forecast = $this->get_forecast( $eventDate, $location );

				if ( $forecast ) {
					// then append it to the existing transient
					$weather[$eventDate] = $forecast;
					set_transient( $loc_transient_name, $weather, 3 * HOUR_IN_SECONDS );
				}

			}

			return $weather[$eventDate];

		}

	}

	/**
	 * Get a forecast from the API for all the days in the requested month for the requested location
	 *
	 * @param   $month          string  The string of the month (YYYY-MM) to return a forecast for.
	 * @param   $location       array   The location array setting from the DB.
	 * @return  $montly_weather array   An array of the days in the month and their associate weather data.
	 * @since   1.0.0
	 */
	public function get_forecast( $month, $location ) {

		$api_url    = 'https://api.forecast.io/forecast/';
		$api_key    = tribe_get_option( 'forecastApiKey' );
		$api_params = '?exclude=currently,minutely,hourly,alerts,flags';
		$monthly_weather = array();

		if ( ! empty( $api_key ) ) {

			// Get the number of days in this month
			$month_parts = explode( '-', $month );
			$days_in_month = cal_days_in_month( CAL_GREGORIAN, $month_parts[1], $month_parts[0] );

			for ( $i = 1; $i <= $days_in_month; $i++ ) {

				// Create the complete date string for the API request
				$date = $month . '-' . str_pad( $i, 2, '0', STR_PAD_LEFT );

				// Compile & make the request
				$request = $api_url . $api_key . '/' . $location[0] . ',' . $location[1] . ',' . $date . 'T12:00:00' . $api_params;
				$response = wp_remote_get( $request );

				// If we got a valid response, compile the response data into an array for this day and add it to the month.
				if ( '200' == wp_remote_retrieve_response_code( $response ) ) {
					$raw_data = json_decode( wp_remote_retrieve_body( $response ), true );
					$weather = array(
						'temp'      => round( $raw_data['daily']['data'][0]['temperatureMax'] ),
						'icon'      => $raw_data['daily']['data'][0]['icon'],
						'summary'   => $raw_data['daily']['data'][0]['summary'],
					);
					$monthly_weather[$date] = $weather;
				}
			}

		}

		return $monthly_weather;

	}

	/**
	 * Get the weather info for a specific day on the calendar.
	 *
	 * @param   $current_day    array   the day we're planning to get weather for.
	 * @return  $current_day    mixed   the day with weather data added.
	 * @since   1.0.0
	 */
	public function get_weather_for_day( $current_day ) {

		$api_key        = tribe_get_option( 'forecastApiKey' );
		$location       = tribe_get_option( 'weatherLatLong' );
		$show_weather   = tribe_get_option( 'showWeather' );

		if ( empty( $api_key ) || empty( $location ) ||empty( $show_weather ) ) {
			return $current_day;
		}

		$current_day['weather'] = array();
		$date = $current_day['date'];

		// We need to get the month of the current day to get weather for
		preg_match( '/\d\d\d\d-\d\d/', $date, $matches );

		// Get the weather for the whole month
		$monthly_weather = $this->get_monthly_forecast( $matches[0] );

		// If we have weather for the month find the current day's weather
		if ( ! empty( $monthly_weather[$date] ) ) {
			$current_day['weather'] = $monthly_weather[$date];
		}

		return $current_day;

	}


}

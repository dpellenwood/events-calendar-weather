<?php

/**
 * Fired during plugin activation
 *
 * @link       http://tri.be
 * @since      1.0.0
 *
 * @package    Events_Calendar_Weather
 * @subpackage Events_Calendar_Weather/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Events_Calendar_Weather
 * @subpackage Events_Calendar_Weather/includes
 * @author     Modern Tribe <info@moderntribe.com>
 */
class Events_Calendar_Weather_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		// TODO: We should really verify that TEC is activated before we run this plugin
	}

}

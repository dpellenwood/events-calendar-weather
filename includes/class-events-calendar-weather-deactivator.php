<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://tri.be
 * @since      1.0.0
 *
 * @package    Events_Calendar_Weather
 * @subpackage Events_Calendar_Weather/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Events_Calendar_Weather
 * @subpackage Events_Calendar_Weather/includes
 * @author     Modern Tribe <info@moderntribe.com>
 */
class Events_Calendar_Weather_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}

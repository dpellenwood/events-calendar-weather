<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * Dashboard. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://tri.be
 * @since             1.0.0
 * @package           Events_Calendar_Weather
 *
 * @wordpress-plugin
 * Plugin Name:       The Events Calendar Weather 
 * Plugin URI:        http://wtfqrcodes.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress dashboard.
 * Version:           1.0.0
 * Author:            Modern Tribe, Inc.
 * Author URI:        http://tri.be
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       events-calendar-weather
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-events-calendar-weather-activator.php
 */
function activate_events_calendar_weather() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-events-calendar-weather-activator.php';
	Events_Calendar_Weather_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-events-calendar-weather-deactivator.php
 */
function deactivate_events_calendar_weather() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-events-calendar-weather-deactivator.php';
	Events_Calendar_Weather_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_events_calendar_weather' );
register_deactivation_hook( __FILE__, 'deactivate_events_calendar_weather' );

/**
 * The core plugin class that is used to define internationalization,
 * dashboard-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-events-calendar-weather.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_events_calendar_weather() {

	$plugin = new Events_Calendar_Weather();
	$plugin->run();

}
run_events_calendar_weather();

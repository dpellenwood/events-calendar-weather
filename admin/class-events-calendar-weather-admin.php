<?php

/**
 * The dashboard-specific functionality of the plugin.
 *
 * @link       http://tri.be
 * @since      1.0.0
 *
 * @package    Events_Calendar_Weather
 * @subpackage Events_Calendar_Weather/admin
 */

/**
 * The dashboard-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Events_Calendar_Weather
 * @subpackage Events_Calendar_Weather/admin
 * @author     Modern Tribe <info@moderntribe.com>
 */
class Events_Calendar_Weather_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $events_calendar_weather    The ID of this plugin.
	 */
	private $events_calendar_weather;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $events_calendar_weather       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $events_calendar_weather, $version ) {

		$this->events_calendar_weather = $events_calendar_weather;
		$this->version = $version;

	}

	/**
	 * Add the weather settings tab
	 *
	 * @return void
	 */
	public function add_settings_tabs() {
		require_once( plugin_dir_path( __FILE__ ) . 'views/tribe-options-weather.php' );
		new TribeSettingsTab( 'weather', __( 'Weather', $this->events_calendar_weather ), $weatherTab );
	}

	/**
	 * Register the stylesheets for the Dashboard.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Events_Calendar_Weather_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Events_Calendar_Weather_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->events_calendar_weather, plugin_dir_url( __FILE__ ) . 'css/events-calendar-weather-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the dashboard.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Events_Calendar_Weather_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Events_Calendar_Weather_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->events_calendar_weather, plugin_dir_url( __FILE__ ) . 'js/events-calendar-weather-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Saves a geocoded address for getting the weather data from the Google Maps API
	 *
	 * @since      1.0.0
	 * @param      string    $value The validated address
	 * @param      string    $field_id The setting filed being validated
	 * @return     string    $value The validated (and possibly standardized) address
	 */
	public function set_address_geocode( $value, $field_id ) {

		// We only want to check the weather address field.
		if( 'addressForWeather' != $field_id ) {
			return $value;
		}

		// If the address hasn't changed, we got nothin' to do!
		$old_address = tribe_get_option( 'addressForWeather' );

		if ( $old_address == $value ) {
			// TODO: Remove the old value's weather transients
			return $value;
		}

		// The complete URL, including the encoded address
		$url = 'http://maps.google.com/maps/api/geocode/json?sensor=false&address=' . urlencode( $value );
		$response = wp_remote_get( $url );

		// If we got a valid response from the the geocoding server
		if ( '200' == wp_remote_retrieve_response_code( $response ) ) {

			$response_body = json_decode( wp_remote_retrieve_body( $response ), true );

			// If the geocoder was able to find a valid location for our address
			if ( 'OK' == $response_body['status'] ) {

				$lat = $response_body['results'][0]['geometry']['location']['lat'];
				$long = $response_body['results'][0]['geometry']['location']['lng'];
				$formatted_address = $response_body['results'][0]['formatted_address'];

				// verify if data is complete
				if ( $lat && $long && $formatted_address ) {

					// put the data in the array
					$data_arr = array();
					array_push( $data_arr, $lat, $long, $formatted_address );

					// Set/update the geocode data option
					tribe_update_option( 'weatherLatLong', $data_arr );

					// Update the address setting option to use the formatted address
					/**
					 * TODO: This doesn't "show" up on the initial settings page response.
					 * Refreshing the page shows it correctly. I'm not sure why (yet).
					 */
					$value = $formatted_address;

				}

			}

		}

		return $value;

	}
}

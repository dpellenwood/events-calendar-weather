<?php

$weatherTab = array(
	'priority' => 25,
	'fields'   => apply_filters(
		'tribe_weather_settings_tab_fields', array(
			'info-start'                         => array(
				'type' => 'html',
				'html' => '<div id="modern-tribe-info">'
			),
			'info-box-title'                     => array(
				'type' => 'html',
				'html' => '<h2>' . __( 'Weather Settings', $this->events_calendar_weather ) . '</h2>',
			),
			'info-box-description'               => array(
				'type' => 'html',
				'html' => '<p>' . __( 'We should probably say something really neat here. Maybe something about how awesome QR codes are?', $this->events_calendar_weather ) . '</p>',
			),
			'info-end'                           => array(
				'type' => 'html',
				'html' => '</div>',
			),
			'tribe-form-content-start'           => array(
				'type' => 'html',
				'html' => '<div class="tribe-settings-form-wrap">',
			),
			'tribeEventsWeatherTitle'            => array(
				'type' => 'html',
				'html' => '<h3>' . __( 'Weather Settings', $this->events_calendar_weather ) . '</h3>',
			),
			'showWeather'               => array(
				'type'            => 'checkbox_bool',
				'label'           => __( 'Display weather forecast', $this->events_calendar_weather ),
				'tooltip'         => __( 'Show the forecasted high temperature and conditions for each day on the calendar view.', $this->events_calendar_weather ),
				'default'         => true,
				'validation_type' => 'boolean',
			),
			'addressForWeather'           => array(
				'type'            => 'text',
				'label'           => __( 'Location for weather', $this->events_calendar_weather ),
				'tooltip'         => __( 'Enter all or part of an address to use as the location for weather data', $this->events_calendar_weather ),
				'size'            => 'large',
				'default'         => '55802',
				'validation_type' => 'address',
			),
			'forecastApiKey'                => array(
				'type'            => 'license_key',
				'label'           => __( 'Forecast.io API Key', $this->events_calendar_weather ),
				'size'            => 'large',
				'tooltip'         => sprintf( __( 'An API key is available from <a href="%s">developer.forecast.io</a>.', $this->events_calendar_weather ), 'https://developer.forecast.io/' ),
				'validation_type' => 'license_key',
			),
			'tribe-form-content-end'             => array(
				'type' => 'html',
				'html' => '</div>',
			),
		)
	)
);
